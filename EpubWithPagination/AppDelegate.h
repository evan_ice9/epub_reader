//
//  AppDelegate.h
//  EpubWithPagination
//
//  Created by ganesh kulpe on 28/04/14.
//  Copyright (c) 2014 xyz. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IndexViewController.h"
#import "ViewController.h"
#import "SlidingViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
