//
//  AppDelegate.m
//  EpubWithPagination
//
//  Created by ganesh kulpe on 28/04/14.
//  Copyright (c) 2014 xyz. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    
    UIStoryboard *storyboard;
    if (IPAD)
    {
        
        storyboard=[UIStoryboard storyboardWithName:@"Main_ipad" bundle:nil];
        NSLog(@"is Ipad");
    }
    else
    {
        storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        NSLog(@"is Iphone");
        
    }
    
    
    IndexViewController *index_vc=(IndexViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ch_vc"];
    
    
    ViewController *vc=(ViewController *)[storyboard instantiateViewControllerWithIdentifier:@"vc_1"];
    
    SlidingViewController *sv=(SlidingViewController *)[storyboard instantiateViewControllerWithIdentifier:@"svc"];
    sv.underLeftViewController=index_vc;
    sv.topViewController=vc;
    sv.anchorRightPeekAmount=250.0;
    
    [vc.view addGestureRecognizer:sv.panGesture];
    
    
    NSLog(@"%@",vc._ePubContent._manifest);
    
    
    self.window.rootViewController=sv;
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
