//
//  SlidingViewController.m
//  EpubWithPagination
//
//  Created by Fahim Ahmed on 9/16/14.
//  Copyright (c) 2014 xyz. All rights reserved.
//

#import "SlidingViewController.h"

@interface SlidingViewController ()

@end

@implementation SlidingViewController


@synthesize menu_status;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(Open_Menu) name:@"open_Menu" object:nil];
    // Do any additional setup after loading the view.
}



-(void)Open_Menu
{

    menu_status=!menu_status;
    
    
    if (menu_status)
    {
        [self anchorTopViewToRightAnimated:YES];
    }
    else
    {
    
        [self resetTopViewAnimated:YES];
    
    }
    
    

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
